import numpy as np
import plotly.graph_objects as go

from scipy.spatial.transform import Rotation
from IPython.display import HTML
from typing import List

def generate_plane():
    """ generate a given list of random triangle of size num_triangle """

    # ------------------------------------------------------------------------------------------------------------------
    # a plane is difined by a point and a normal vector
    # ------------------------------------------------------------------------------------------------------------------
    center_point = np.array([0, 0, np.random.rand() - .5])  # random height
    normal_vect = np.array([0, 0, 1])

    # ------------------------------------------------------------------------------------------------------------------
    # four corners of the plan used for the display
    # ------------------------------------------------------------------------------------------------------------------
    plane_vertices = np.array([[-1000, -1000, 0], [1000, 1000, 0], [-1000, 1000, 0], [1000, -1000, 0]])

    # ------------------------------------------------------------------------------------------------------------------
    # random rotation of the plane
    # ------------------------------------------------------------------------------------------------------------------
    rotation = Rotation.from_euler('xyz', np.random.rand(3) * 360, degrees=True)

    return center_point, rotation.apply(normal_vect), rotation.apply(plane_vertices) + center_point

def read_mesh_obj(path):

    # ------------------------------------------------------------------------------------------------------------------
    # init the vertices and the faces
    # ------------------------------------------------------------------------------------------------------------------
    vtx = []
    face = []

    # ------------------------------------------------------------------------------------------------------------------
    # parse the obj file
    # ------------------------------------------------------------------------------------------------------------------
    with open(path, "r") as f:

        for line in f.readlines():

            if line[0:2] == "v ":
                x, y, z = line[2:-1].split(' ')
                vtx.append([float(x), float(y), float(z)])
            if line[0:2] == "f ":
                i, j, k = line[2:-1].split(' ')
                face.append([int(i.split('/')[0]), int(j.split('/')[0]), int(k.split('/')[0])])

    vtx = np.array(vtx)
    face = np.array(face)-1

    # ------------------------------------------------------------------------------------------------------------------
    # scaling of the vertices in the bbox [-1,1]^3
    # ------------------------------------------------------------------------------------------------------------------
    vtx -= vtx.mean(0)
    vtx /= np.max(np.abs(vtx))

    return vtx, face

def visualize(vtx: np.array, face: np.array, plane: List[np.array], label_list: np.array):

    # ------------------------------------------------------------------------------------------------------------------
    # init triangle mesh to diplay
    # ------------------------------------------------------------------------------------------------------------------
    mesh = [
        go.Mesh3d(
            x=[plane[0, 0], plane[1, 0], plane[2, 0], plane[3, 0]],  # triangle coordinates 
            y=[plane[0, 2], plane[1, 2], plane[2, 2], plane[3, 2]],
            z=[plane[0, 1], plane[1, 1], plane[2, 1], plane[3, 1]],
            color='grey',
            opacity=0.2,
        )
    ]

    # ------------------------------------------------------------------------------------------------------------------
    # convertion from label to color
    # ------------------------------------------------------------------------------------------------------------------
    label = np.zeros(len(face)) if label_list is None else label_list
    label_2_rgb = {-1:'blue', 0:'red', 1:'yellow'}
    facecolor = [label_2_rgb[l] for l in label]
    mesh.append(go.Mesh3d(x=vtx[:, 0],
                          y=vtx[:, 2],
                          z=vtx[:, 1],
                          i=face[:, 0],
                          j=face[:, 1],
                          k=face[:, 2],
                          facecolor=facecolor,
                          opacity=.3,
                         )
               )

    # ------------------------------------------------------------------------------------------------------------------
    # overall fugure
    # ------------------------------------------------------------------------------------------------------------------
    fig = go.Figure(data=mesh)
    fig.update_layout(
        autosize=True,
        height=800,
        template='simple_white',
        scene = dict(
            xaxis = dict(range=[-1.5, 1.5], visible=False),
            yaxis = dict(range=[-1.5, 1.5],visible=False),
            zaxis =dict(range=[-1.5, 1.5],visible=False),
            aspectmode='cube'
        ),
    )


    return HTML(fig.to_html())
